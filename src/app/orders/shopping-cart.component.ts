import { Component } from "@angular/core";
import { Order } from "./order";
import { Product } from "../products/product";
import { OrderService } from "./order.service";

@Component({
    selector: "shopping-cart",
    templateUrl: "./shopping-cart.component.html",
    styleUrls: ["./shopping-cart.styles.css"]
})
export class ShoppingCartComponent {
    order: Order;

    constructor(private orderService: OrderService) {
        this.order = orderService.order;
    }

    removeFromCart(id: number) {
        this.orderService.removeFromCart(id);
    }
}