import { Component } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { Router } from "@angular/router";
import { OrderService } from "./order.service";
import { User } from "../user";
import { OrderResult } from "./order-result";

@Component({
    selector: "checkout-form",
    templateUrl: "./checkout-form.component.html"
})
export class CheckoutFormComponent {
    user: User = new User();

    constructor(private router: Router, private orderService: OrderService) {
    }

    confirmOrder() {
        this.orderService.confirmOrder(this.user).subscribe(
            (order: OrderResult) => {
                let orderNumber = order.orderid;
                this.router.navigate(["potvrzeni-objednavky", orderNumber]);
            },
            (err) => {
                
            }
        );

    }
}