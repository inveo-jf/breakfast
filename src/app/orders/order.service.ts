import { EventEmitter, Injectable, Inject, Output } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Product } from "../products/product";
import { Order } from "./order";
import { OrderResult } from "./order-result";
import { User } from "../user";
import { API_CONFIG, IApiConfig } from "../api.config";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

@Injectable()
export class OrderService {
    order: Order;
    @Output() productAdded: EventEmitter<number> = new EventEmitter<number>();
    @Output() productRemoved: EventEmitter<number> = new EventEmitter<number>();

    /**
     * Sluzba ke zpracovani objednavek
     */
    constructor(private http: Http, @Inject(API_CONFIG) private apiConfig: IApiConfig) {
        console.log(apiConfig);
        this.resetOrder();
    }

    addToCart(product: Product) {
        this.order.products.push(product);
        this.productAdded.emit(product._id);
    }

    removeFromCart(id: number) {
        let index: number = this.order.products
            .findIndex(i => i._id === id);
        if (index === -1) {
            return;
        }
        this.order.products.splice(index, 1);
        this.productRemoved.emit(id);
    }

    confirmOrder(user: User): Observable<any> {
        this.order.name = user.name;
        this.order.email = user.email;

        let url = this.apiConfig.baseUrl + "order";
        let body = JSON.stringify(this.order);

        let header = new Headers();
        header.append("apikey", this.apiConfig.apiKey);
        header.append("Content-Type", "application/json");

        return this.http.post(url, body, { headers: header })
        .map(res => {
            this.resetOrder();
            return <OrderResult>res.json();
        });
    }

    private resetOrder() {
        this.order = new Order();
    }
}