import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";

import { ShoppingCartComponent } from "./shopping-cart.component";
import { CheckoutComponent } from "./checkout.component";
import { CheckoutFormComponent } from "./checkout-form.component";
import { OrderService } from "./order.service";
import { OrderConfirmationComponent } from "./order-confirmation.component";

@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule],
    declarations: [CheckoutComponent, CheckoutFormComponent, ShoppingCartComponent, OrderConfirmationComponent],
    exports: [CheckoutComponent, CheckoutFormComponent, ShoppingCartComponent, OrderConfirmationComponent],
    providers: [OrderService]
})
export class OrderModule {
    /**
     * Modul objednavek.
     */
    constructor() {
    }
}