import {Component} from "@angular/core";
import {OrderService} from "./order.service";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: "order-confirmation",
    templateUrl: "./order-confirmation.component.html"
})
export class OrderConfirmationComponent {
    orderNumber: number

    /**
     * Potvrzeni objednavky - zakaznik dostane cislo
     */
    constructor(route: ActivatedRoute, orderService: OrderService) {
        this.orderNumber = +route.snapshot.params["number"];
    }

    ngOnInit() {
        scrollTo(0, 0);
    }
}