import { Product } from "../products/product";

export class Order {
    products: Product[];
    name: string;
    email: string;

    get totalPrice(): number {
        let sum: number = 0;
        this.products.forEach(i => sum += i.price);
        return sum;
    }

    constructor() {
        this.products = [];
    }
}