import { Component, Input } from "@angular/core";
import { Product } from "./product";
import { OrderService } from "../orders/order.service";

@Component({
    selector: "product-item",
    templateUrl: "./product-item.component.html",
    styleUrls: ["./product-item.styles.css"]
})
export class ProductItemComponent {
    @Input() product: Product;
    canBeAdded: boolean = true;

    /**
     * Polozka v seznamu produktu.
     */
    constructor(private orderService: OrderService) {
        // orderService.productAdded.subscribe((id: number) => {
        //     if (id === this.product._id) this.isAdded = true;
        // })
        // orderService.productRemoved.subscribe((id: number) => {
        //     if (id === this.product._id) this.isAdded = false;
        // })
    }

    ngOnInit() {
        let index: number = this.orderService.order.products.findIndex(p => p._id === this.product._id);
    }

    addToCart(product: Product) {
        this.orderService.addToCart(product);
        this.canBeAdded = false;
        setTimeout(() => {
            this.canBeAdded = true;
        }, 500);
    }
}