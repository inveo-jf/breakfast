import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { ProductService } from "./product.service";
import { ProductListComponent } from "./product-list.component";
import { ProductItemComponent } from "./product-item.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [ProductItemComponent, ProductListComponent],
    exports: [ProductItemComponent, ProductListComponent],
    providers: [ProductService]
})
export class ProductModule {
}