import { Category } from "./category";

export class Product {
    constructor(public _id: number, public productname: string, public price: number, public description: string, public imageurl: string) {
    }
}