import { Component, Injectable, Inject } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Product } from "./product";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { API_CONFIG, IApiConfig } from "../api.config";

@Injectable()
export class ProductService {
    baseUrl: string;
    apiKey: string;
    
    constructor(private http: Http, @Inject(API_CONFIG) apiConfig: IApiConfig) {
        // apiConfig se vyhodnoti v runtime, takze nezname jeho typ
        // Muzeme ale poradit TypeScriptu a tim i sobe
        this.baseUrl = apiConfig.baseUrl;
        this.apiKey = apiConfig.apiKey;
    }

    getProducts(): Observable<Product[]> {
        const headers = new Headers();
        headers.append("apiKey", this.apiKey);

        return this.http.get(this.baseUrl + "product",
            { headers: headers }).map((res: Response) => res.json());
    }
}