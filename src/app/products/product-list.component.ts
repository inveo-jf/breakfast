import { Component } from "@angular/core";
import { Product } from "./product";
import { ProductService } from "./product.service";

@Component({
    selector: "product-list",
    templateUrl: "./product-list.component.html"
})
export class ProductListComponent {
    products: Product[];
    /**
     * Seznam produktu na strance.
     */
    constructor(private productService: ProductService) {}

    ngOnInit() {
        this.productService.getProducts().subscribe(
            (products) => this.products = products
        );
    }
}