import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { HttpModule } from "@angular/http";

import { AppComponent } from './app.component';
import { OrderModule } from "./orders/order.module";
import { ProductModule } from "./products/product.module";
import { routes } from "./app.routes";
import { API_CONFIG, ApiConfig } from "./api.config";
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    OrderModule,
    HttpModule,
    ProductModule,
    RouterModule.forRoot(routes)
  ],
  providers: [{ provide: API_CONFIG, useClass: ApiConfig }],
  bootstrap: [AppComponent]
})
export class AppModule { }
