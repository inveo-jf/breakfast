// Kdyby nahodou nekdo pouzil token se stejnym textem, doslo by ke kolizi. Proto ma Angular tridu InjectionToken, ktera zajisti unikatnost.
import { InjectionToken } from "@angular/core";
export const API_CONFIG = new InjectionToken("api.config");

export interface IApiConfig {
    apiKey: string;
    baseUrl: string;
}

export class ApiConfig implements IApiConfig {
    apiKey: string = "59550192afce09e87211e959";
    baseUrl: string = "https://snidane-c938.restdb.io/rest/";
}