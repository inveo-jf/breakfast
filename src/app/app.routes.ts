import { Routes } from "@angular/router";
import { CheckoutComponent } from "./orders/checkout.component";
import { HomeComponent } from "./home/home.component";
import { OrderConfirmationComponent } from "./orders/order-confirmation.component";

export const routes: Routes = [
    {
        path: "kosik",
        component: CheckoutComponent
    },
    {
        path: "",
        component: HomeComponent
    },
    {
        path: "potvrzeni-objednavky/:number",
        component: OrderConfirmationComponent
    },
    {
        path: "**",
        redirectTo: "/"
    },
]